# DiscordOwExbot

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `discord_ow_exbot` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:discord_ow_exbot, "~> 0.1.0"}]
    end
    ```

  2. Ensure `discord_ow_exbot` is started before your application:

    ```elixir
    def application do
      [applications: [:discord_ow_exbot]]
    end
    ```

