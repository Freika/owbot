require 'discordrb'
require 'httparty'
require 'russian'

token = 'MjQ4MDM5Mjg5NDQ5ODczNDA5.CwyMdQ.uZfPhYpaYukcCn3vbQV-lyFmbE4'
client_id = 248039289449873409

bot = Discordrb::Commands::CommandBot.new(
  token: token, client_id: client_id, prefix: '!'
)

BRONZE      = 1..1499
SILVER      = 1500..1999
GOLD        = 2000..2499
PLATINUM    = 2500..2999
DIAMOND     = 3000..3499
MASTER      = 3500..3999
GRANDMASTER = 4000

####################################

bot.command(:rating, usage: '!rating Battletag-1020') do |event, battletag|
  now = DateTime.now
  end_of_3rd_season = DateTime.new(2017, 2, 28, 00, 00)

  till_end = (end_of_3rd_season.mjd - now.mjd)

  url = "https://api.lootbox.eu/pc/eu/#{battletag}/profile"

  begin
    response = HTTParty.get(url)

    rating = response['data']['competitive']['rank'].to_i

    case rating
    when BRONZE
      league = 'Бронзовой лиге'
      left = BRONZE.last - rating
      next_league = 'Серебряная лига'
      next_next_league = 'Золотая лига'
      next_left = SILVER.last - rating
    when SILVER
      league = 'Серебряной лиге'
      left = SILVER.last - rating
      next_league = 'Золотая лига'
      next_next_league = 'Платиновая лига'
      next_left = GOLD.last - rating
    when GOLD
      league = 'Золотой лиге'
      left = GOLD.last - rating
      next_league = 'Платиновая лига'
      next_next_league = 'Алмазная лига'
      next_left = PLATINUM.last - rating
    when PLATINUM
      league = 'Платиновой лиге'
      left = PLATINUM.last - rating
      next_league = 'Алмазная лига'
      next_next_league = 'Лига мастеров'
      next_left = DIAMOND.last - rating
    when DIAMOND
      league = 'Алмазной лиге'
      left = DIAMOND.last - rating
      next_league = 'Лига мастеров'
      next_next_league = 'Лига Грандмастеров'
      next_left = MASTER.last - rating
    when MASTER
      league = 'Лиге Мастеров'
      left = MASTER.last - rating
      next_league = 'Лига Грандмастеров'
    when GRANDMASTER
      league = 'Лиге Грандмастеров'
      left = GRANDMASTER.last - rating
    else
      not_qualified = 'Вы еще не квалифицированы в текущем сезоне. Пройдите квалификацию!'
    end

    handled_in = (Time.now - event.timestamp).round(2)
    seconds = days = Russian.p(handled_in, 'секунду', 'секунды', 'секунд', 'секунды')

    event << "#{battletag} обработан за #{handled_in} #{seconds}."

    if not_qualified
      event << not_qualified
    else
      per_day = left / till_end
      next_per_day = next_left / till_end if next_next_league

      days = Russian.p(till_end, 'день', 'дня', 'дней')

      event << "Третий сезон заканчивается через #{till_end} #{days}."
      event << "Текущий соревновательный рейтинг: #{rating}."
      event << "Вы в #{league}, следующая — #{next_league}."
      event << "Чтобы попасть в неё, нужно получать не менее #{per_day} очков рейтинга в день."
      if next_next_league
        event << "Чтобы попасть в еще более лучшую лигу, нужно получать не менее #{next_per_day} очков рейтинга в день"
      end
    end

  rescue => e
    event << 'Что-то пошло не так ¯\_(ツ)_/¯'
    event << "Детали: #{e}"
  end
end

####################################

bot.command :user do |event|
  # Commands send whatever is returned from the block to the channel. This allows for compact commands like this,
  # but you have to be aware of this so you don't accidentally return something you didn't intend to.
  # To prevent the return value to be sent to the channel, you can just return `nil`.
  event.user.name
end

bot.command(:invite, chain_usable: false) do |event|
  # This simply sends the bot's invite URL, without any specific permissions,
  # to the channel.
  event.bot.invite_url
end

bot.run
